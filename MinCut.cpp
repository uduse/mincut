// MinCut.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Node.h"
#include <ctime>
#include <fstream>
#include <iosfwd>
#include <iostream>
#include <queue>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

int _tmain( int argc, _TCHAR* argv[] )
{
	clock_t begin = clock();
	string tempS;
	int temp;
	srand( time( NULL ) );

	fstream in( "kargerMinCut.txt" );

	Node::mapSize = NUM_VERTICES;
	Node::map = new Node*[NUM_VERTICES];

	for ( int i = 0; i < NUM_VERTICES; i++ )
	{
		Node::map[i] = new Node();
	}

	while ( getline( in, tempS ) )
	{
		istringstream iss1( tempS );
		istringstream iss2( tempS );

		// Which Node
		iss1 >> temp;
		Node* ptr = Node::map[temp - 1];

		// Count Adjacents
		int count = 0;
		while ( iss1 >> temp )
		{
			count++;
		}

		// Allocate Space
		ptr->adjacent = new Node*[count];
		ptr->adjSize = count;

		// Assign Adjacents
		iss2 >> temp;
		int index = 0;
		while ( iss2 >> temp )
		{
			ptr->adjacent[index] = Node::map[temp - 1];
			index++;
		}
	}

	int mergeCount = 0;
	int minCut = NUM_VERTICES*NUM_VERTICES;

	int times = (int)( NUM_VERTICES * NUM_VERTICES * log( NUM_VERTICES ) );
	times = 5000;
	cout << "times: " << times << endl;

	for ( int i = 0; i < times; i++ )
	{
		// Merging
		while ( mergeCount < NUM_VERTICES - 2 )
		{
			randomMerge();
			mergeCount++;
		}

		// Counting
		int newCount = queueGroupA_CountCross();

		// Post-Processing
		if ( newCount == 0 )
		{
			cout << "!!!" << endl;
		}
		if ( newCount < minCut )
		{
			minCut = newCount;
		}
		init();
		mergeCount = 0;

		// Progress Bar
		if ( i % ( times / 100 + 1 ) == 0 )
		{
			cout << "Progress: " << i * 100 / times << "%" << endl;
		}
	}

	cout << "minCut: " << minCut << endl;

	clock_t end = clock();
	double elapsed_secs = double( end - begin ) / CLOCKS_PER_SEC;
	cout << endl;
	cout << "Running Time: " << elapsed_secs; cout << endl;
	return 0;
}