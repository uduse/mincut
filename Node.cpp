#include "stdafx.h"
#include "Node.h"

int Node::mapSize;
Node* Node::bRoot;
Node** Node::map;
queue<Node*> Node::todo;
vector<Node*>::iterator itr;

using namespace std;

Node::Node():parent( NULL ), height( 1 )
{
}

Node::~Node()
{
}

Node* find( Node* &x )
{
	{
		if ( !x->parent )
		{
			return x;
		}
		else
		{
			// Path Contraction
			if ( x->parent->parent )
			{
				x->parent = x->parent->parent;
			}
			return find( x->parent );
		}
	}
}
bool merge( Node* &x, Node* &y )
{
	Node* rootX = find( x );
	Node* rootY = find( y );
	if ( rootX == rootY )
	{
		return false;
	}
	else
	{
		// Link x, y
		if ( x->height >= y->height )
		{
			rootY->parent = rootX;
			int newBranchHeight = rootY->height + 1;
			rootX->height = ( rootX->height > newBranchHeight ? rootX->height : newBranchHeight );
		}
		else
		{
			rootX->parent = rootY;
			int newBranchHeight = rootX->height + 1;
			rootY->height = ( rootY->height > newBranchHeight ? rootY->height : newBranchHeight );
		}
		return true;
	}
}

void init()
{
	int adjSize = Node::mapSize;
	for ( int i = 0; i < adjSize; i++ )
	{
		Node::map[i]->parent = NULL;
	}
}

void randomMerge()
{
	bool notFound = true;
	//Node* x = NULL;
	//Node* y = NULL;
	while ( notFound )
	{
		Node* x = Node::map[rand() % NUM_VERTICES];

		int adjSize = x->adjSize;
		for ( int i = 0; i < adjSize; i++ )
		{
			if ( find( x ) != find( x->adjacent[i] ) )
			{
				merge( x, x->adjacent[i] );
				notFound = false;
				break;
			}
		}
	}
}
int queueGroupA_CountCross()
{
	Node* parentA = find( Node::map[0] );
	int count = 0;
	int index;
	int adjSize;

	// Find Root A and Root B
	for ( index = 0; index < Node::mapSize; index++ )
	{
		if ( find( Node::map[index] ) == parentA )
		{
			Node::todo.push( Node::map[index] );
		}
		else
		{
			Node::bRoot = find( Node::map[index] );
			index++;
			break;
		}
	}

	// Process Nodes left in Group A
	while ( !Node::todo.empty() )
	{
		Node* source = Node::todo.front();
		adjSize = source->adjSize;
		for ( int i = 0; i < adjSize; i++ )
		{
			Node* childRoot = find( source->adjacent[i] );
			if ( childRoot == Node::bRoot )
			{
				count++;
			}
		}
		Node::todo.pop();
	}

	for ( ; index < Node::mapSize; index++ )
	{
		if ( find( Node::map[index] ) == parentA )
		{
			Node* source = Node::map[index];
			adjSize = source->adjSize;
			for ( int i = 0; i < adjSize; i++ )
			{
				Node* childRoot = find( source->adjacent[i] );
				if ( childRoot == Node::bRoot )
				{
					count++;
				}
			}
		}
	}

	return count;
}