#pragma once
#include <vector>
#include <stack>
#include <queue>
#include <iostream>

//#define NUM_VERTICES 8
//#define NUM_VERTICES 40
#define NUM_VERTICES 200

using namespace std;

class Node
{
public:
	~Node();
	friend bool merge( Node* &x, Node* &y );
	friend int queueGroupA_CountCross();
	friend Node* find( Node* &x );
	friend void init();
	friend void randomMerge();
	int adjSize;
	int height;

	Node();
	Node* parent;
	Node** adjacent;
	static int mapSize;
	static Node* bRoot;
	static Node** map;
	static queue<Node*> todo;

private:

};

