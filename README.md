##Min Cut Problem Solver

A random contraction algorithm that compute the minimum cut (the minimum possible number of edges need to break in order to separate a graph into two) of a graph.

##Things I learned from this project

I wrote solvers to this problem twice. 

The first time I wrote it I didn't know anything about data structure. My implementation was horribly naive and inefficient. Since the random contraction algorithm requires n\*n\*log(n) iterations to find a proper min cut, with a large input file, it took my program **5 hours** to solve to problem.

The second time, namely this time, I, with some knowledge of data structure and algorithm, wrote a much better one using **union find**. My first working version gave me a running time about **250s**, which is already much much better than 5 hours I had. However, I wanted to do better. I then applied **path contraction** and **merge by weight/height** and many other optimization. By doing so, I successfully squeezed my running time in **115s**. Had fun, satisfied.

1. **Union Find is super awesome!**
3. **Path Contraction is awesome.** Saves a lot of findings.
3. **Merge by Weight/Height, sweet to have.** In my case merging by height is slightly better.
4. **Learned how to use "Performance Diagnostics" in Visual Studio**
6. **Iterators are slow.** Traversing an array (vector) using an iterator is my habit, but the "++" operator of it seems to take substantial time. Replaced it with accessing elements by direct indexing.
7. **Vectors are slow.** Replaced all of them by dynamic arrays.